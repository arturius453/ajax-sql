function onButton() {
    var mainSelect = document.getElementById('2');
    var xmlHttp = new XMLHttpRequest();
    switch (mainSelect.selectedIndex) {
        case 0:
            xmlHttp.onload = function () {
                document.getElementById('table').
                    innerHTML = xmlHttp.responseText;
            };
            break;
        case 1:
            xmlHttp.responseType = 'document';
            xmlHttp.onload = function () {
                var xmlDoc = xmlHttp.responseXML;
                var computers = xmlDoc.getElementsByTagName('computers');
                var str = '';
                if (computers[0].childNodes[0].childNodes.length == 0)
                    str = "Немає таких комп'ютерів";
                str += '<tr>';
                for (var i = 0; i < computers[0].childNodes[0].childNodes.length; i++) {
                    str += '<td>' +
                        computers[0].childNodes[0].childNodes[i].nodeName
                        + '</td>';
                }
                str += '</tr>';
                //alert(computers[0].childNodes.length);
                for (var i = 0; i < computers[0].childNodes.length; i++) {
                    str += '<tr>';
                    for (var j = 0; j < computers[0].childNodes[i].childNodes.length; j++)
                        str += '<td>'
                            + computers[0].childNodes[i].childNodes[j].childNodes[0].nodeValue
                            + '</td>';
                    str += '</tr>';
                }
                document.getElementById('table').innerHTML = str;
            };
            break;
        case 2:
            xmlHttp.onload = function () {
                var jsonData = JSON.parse(xmlHttp.responseText);
                var str = '';
                str += '<tr>';
                Object.keys(jsonData[0]).forEach(function (key) {
                    str += '<td>' + key + '</td>';
                });
                str += '</tr>';
                var _loop_1 = function (i) {
                    str += '<tr>';
                    Object.keys(jsonData[i]).forEach(function (key) {
                        str += '<td>' + jsonData[i][key] + '</td>';
                    });
                    str += '</tr>';
                };
                for (var i = 0; i < jsonData.length; i++) {
                    _loop_1(i);
                }
                document.getElementById('table').innerHTML = str;
            };
            break;
    }
    xmlHttp.open("POST", "subm.php", true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var procTypes = document.getElementById('0');
    var softTypes = document.getElementById('1');
    var ms = mainSelect.options[mainSelect.selectedIndex].text;
    var pt = procTypes.options[procTypes.selectedIndex].text;
    var st = softTypes.options[softTypes.selectedIndex].text;
    var str = "mainSelect=" + ms + "&procTypes=" + pt + "&softTypes=" + st;
    xmlHttp.send(str);
}
